#include <stdlib.h> // > cbx.ppm
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <x86intrin.h>
#include <semaphore.h>
#include <pthread.h>

const float TAU = 6.283185307179586f;

inline float _f(const int &i) {
  return reinterpret_cast<const float&>(i);
}

struct Vec {
    __m128 data;

    Vec(const Vec &other) : data(other.data) { }

    Vec(float v = 0): data(_mm_set1_ps(v)) { }

    Vec(float a, float b, float c = 0) : data(_mm_setr_ps(a, b, c, 0.0f)) { }

    explicit Vec(__m128 d) : data(d) { }

    inline Vec operator+(const Vec &r) const { return Vec(_mm_add_ps(data, r.data)); }

    inline Vec operator-(const Vec &r) const { return Vec(_mm_sub_ps(data, r.data)); }

    inline Vec operator*(const Vec &r) const { return Vec(_mm_mul_ps(data, r.data)); }

    inline Vec operator/(const Vec &r) const { return Vec(_mm_div_ps(data, r.data)); }

    inline float operator|(const Vec &r) const { return _mm_cvtss_f32(_mm_dp_ps(data, r.data, 0x71)); }

    inline Vec sqrt() const {
        return Vec(_mm_sqrt_ps(data));
    }

    inline void normalize() {
        __m128 f = _mm_set1_ps(1.0f / sqrtf(_mm_cvtss_f32(_mm_dp_ps(data, data, 0x71))));
        data = _mm_mul_ps(data, f);
    }

    // normalize
    inline Vec operator!() const {
      return *this * (1 / sqrtf(*this | *this));
    }
    
    float length2() const {
      return *this | *this;
    }
    
    float length() const {
      return sqrtf(length2());
    }

    inline Vec min(const Vec &r) const { return Vec(_mm_min_ps(data, r.data)); }

    inline float x() const { return _mm_cvtss_f32(data); }
    inline float y() const { return _f(_mm_extract_ps(data, 1)); }
    inline float z() const { return _f(_mm_extract_ps(data, 2)); }

    void flatten(float *f) const { _mm_storeu_ps(f, data); }
};

struct Random {
    int64_t seed;

    int64_t rand() {
        return seed = (seed * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
    }

    float randomVal() {
        return (rand() & ((1 << 24) - 1)) / (float) (1 << 24);
    }
};

float min(float a, float b) { return a < b ? a : b; }

float boxTest(const Vec &pos, const Vec &mins, const Vec &maxs) {
    Vec d1(pos - mins);
    Vec d2(maxs - pos);
    float f[4];
    d1.min(d2).flatten(f);
    return min(min(f[0], f[1]), f[2]);
}

float columnTest(const Vec &pos, const Vec &bottomCenter, float r, float height) {
    float ymin = bottomCenter.y();
    float ymax = ymin + height;
    float p[4];
    pos.flatten(p);
    float d1(p[1] - ymin);
    d1 = -min(d1, ymax - p[1]);
    Vec v = Vec(p[0], bottomCenter.y(), p[2]) - bottomCenter;
    float d2 = sqrtf(v | v) - r;
    if (d2 > d1)
        return d2;
    return d1;
}

enum HitType {
    HIT_WHITE,
    HIT_RED,
    HIT_GREEN,
    HIT_GOLD,
    HIT_LIGHT,
};

const float rc = 0.8660254f, rs = -0.5f;
const Vec mx(rc, 0.0f, rs);
const Vec mz(-rs, 0.0f, rc);

float scene(const Vec &pos, int &type) {
    type = HIT_WHITE;
    // room and (rotated) box
    float minDist = min(boxTest(pos, Vec(-10, -10, -10), Vec(10, 10, 10)),
	        -boxTest(mx * pos.x() + Vec(0.0f, pos.y()) + mz * pos.z(), Vec(3, 6, -3), Vec(7, 10, 1)));
    // doorway
    minDist = -min(-minDist,
        -boxTest(pos, Vec(-3.5, -3, -12.5), Vec(3.5, 10, -9)));
    // other room
    minDist = -min(-minDist, -boxTest(pos, Vec(-10, -10, -22), Vec(10, 10, -12)));
    // column
//    minDist = min(minDist, columnTest(pos, Vec(0, -10, 0), 1.0f, 3.0f));
    float sphereDist = (pos - Vec(-6, 7, 5)).length() - 3.0f;
    if (sphereDist < minDist) minDist = sphereDist, type = HIT_GOLD;
    float p[4];
    pos.flatten(p);
    if (type == HIT_WHITE && p[2] < 10.0f) {
		if (p[0] < -9.9f)
		    type = HIT_RED;   // red wall
		if (p[0] > 9.9f)
		    type = HIT_GREEN;   // green wall
		if (p[1] < -9.9f) {
            if (fabsf(p[0]) <= 5.0f && fabs(p[2]) <= 5.0f)
                type = HIT_LIGHT;
			// Vec v(fabsf(p[0]), fabsf(p[2]));
			// float d = v | v;
			// if (d < 25.0f && d > 16.0f) { 
			//     type = HIT_LIGHT;
			// }
		}
    }
    return minDist;
}

int march(const Vec &pos, const Vec &dir, Vec &hitPos, Vec &hitNorm) {
    int type = 0;
    int noHitCount = 0;
    float d;
    for (float traveled = 0.0f; traveled < 100.0f; traveled += d) {
        if ((d = scene(hitPos = pos + dir * traveled, type)) < 0.01f || ++noHitCount > 99) {
            // noHitCount is not used anymore, and we don't care about the result
            hitNorm = Vec(
                scene(hitPos + Vec(0.01f, 0.0f, 0.0f), noHitCount) - d,
                scene(hitPos + Vec(0.0f, 0.01f, 0.0f), noHitCount) - d,
                scene(hitPos + Vec(0.0f, 0.0f, 0.01f), noHitCount) - d
            );
            hitNorm.normalize();
            return type;
        }
    }
    return 0;
}

Vec tracePath(Random &r, Vec origin, Vec direction, int bounceCount = 3) {
    Vec sampledPosition, normal, color, attenuation = 1;
    bool goldBounceAdded = false;
    while (bounceCount--) {
        int hitType = march(origin, direction, sampledPosition, normal);
        if (hitType == HIT_WHITE || hitType == HIT_GREEN || hitType == HIT_RED) {
            float n[4];
            normal.flatten(n);
            float p = TAU * r.randomVal();
            float c = r.randomVal();
            float s = sqrtf(1 - c);
            float g = n[2] < 0 ? -1 : 1;
            float u = -1 / (g + n[2]);
            float v = n[0] * n[1] * u;
            direction = Vec(v,
                            g + n[1] * n[1] * u,
                            -n[1]) * (cosf(p) * s)
                        +
                        Vec(1 + g * n[0] * n[0] * u,
                            g * v,
                            -g * n[0]) * (sinf(p) * s) + normal * sqrtf(c);
            origin = sampledPosition + direction * 0.1f;
            direction.normalize();
            if (hitType == HIT_WHITE)
                attenuation = attenuation * 0.3f;//0.2;
            else if (hitType == HIT_RED)
                attenuation = attenuation * Vec(0.2f, 0.01f, 0.01f);
            else if (hitType == HIT_GREEN)
                attenuation = attenuation * Vec(0.01f, 0.2f, 0.01f);
        }
        if (hitType == HIT_GOLD) {
            if (!goldBounceAdded) {
                goldBounceAdded = true;
                ++bounceCount;
            }
            direction = direction - normal * (2.0f * (direction | normal));
            direction.normalize();
            origin = sampledPosition + direction * 0.1f;
            direction = direction + Vec(r.randomVal()*0.2f-0.1f, r.randomVal()*0.2f-0.1f, r.randomVal()*0.2f-0.1f);
            direction.normalize();
            const float base = 0.8f;
            attenuation = attenuation * Vec(0.98f*base, 0.72f*base, 0.16f*base);
        }
        if (hitType == HIT_LIGHT) {
            color = color + attenuation * Vec(50, 80, 100);
            break;
        }
    }
    return color;
}

class Renderer;

struct ThreadLocals {
    pthread_t thread;
    sem_t restart;
    sem_t ready;
    Random random;
    Renderer *renderer;
    bool sync;

    void* renderThread();
};

void* renderThread(void *localsPtr) {
    return static_cast<ThreadLocals*>(localsPtr)->renderThread();
}

class Renderer {
    static const int numThreads = 4;
    friend ThreadLocals;
    const int w, h, samplesCount;
    Vec camera, right, up, forward;
    uint8_t *row;
    int x, y;
    float focalLength, aperture, focusDistance, imageDistance, ipOffsetMultiplier;
    Random commonRandom;
    ThreadLocals threads[numThreads];

    void renderPixel(Random &r, int x, int y) {
        uint8_t *c = row + (w - 1 - x)*3;
        Vec color = Vec(0.0f);
        for (int i = samplesCount; --i;) {
            // this is the subpixel we are calculating
            float dx = r.randomVal() - 0.5f;
            float dy = r.randomVal() - 0.5f;
            Vec dir = right * (2.0f * (x + dx) / w - 1) + up * (1.0f - 2.0f * (y + dy) / h) + forward;
            // dir is now projected on the focal plane
            Vec focalPoint = camera + dir * focusDistance;
            Vec ip(r.randomVal(), r.randomVal());
            ip = ip.sqrt()*ipOffsetMultiplier;
            float angle = r.randomVal() * TAU;
            ip = ip * Vec(cosf(angle), sinf(angle));
            Vec imagePlanePixel = camera + dir + right * ip.x() + up * ip.y();
            dir = focalPoint - imagePlanePixel;
            dir.normalize();
            color = color + tracePath(r, imagePlanePixel, dir);
        }
        color = color * (1.0f / samplesCount) + 14.0f / 241.0f;
        Vec o = color + 1.0f;
        color = color / o * 255.0f;
        *c++ = (int) color.x();
        *c++ = (int) color.y();
        *c++ = (int) color.z();
    }
public:
    Renderer(int w, int h, int samplesCount) : w(w), h(h), samplesCount(samplesCount),
        camera(0.0f, 0.0f, -10.8f),
        right((float) w / h, 0.0f),
        up(0.0f, 1.0f),
        forward(0.0, 0.0, 1.0),
        row(new uint8_t[w*3]),
        focalLength(36.0f / (2.0f * right.x())),
        aperture(1.2f),
        focusDistance(15.8f),
        imageDistance(1.0f),
        ipOffsetMultiplier(focalLength / (18.0f * 2.0f) / aperture) {
        commonRandom.seed = reinterpret_cast<int64_t>(clock());
        for (int i = numThreads; i--; ) {
            ThreadLocals &t(threads[i]);
            t.renderer = this;
            t.random.seed = commonRandom.seed;
            sem_init(&t.ready, 0, 0);
            sem_init(&t.restart, 0, 0);
            t.sync = i == 0;
            if (!t.sync)
                pthread_create(&t.thread, 0, renderThread, &t);
        }
    }

    ~Renderer() {
        delete[] row;
        row = 0;
    }

    void dumpParameters() {
        fprintf(stderr, "Rendering %dx%d (samples: %d)\n", w, h, samplesCount);
        fprintf(stderr, "Aperture: f/%.2f\n", aperture);
        fprintf(stderr, "Focal length: %.2f\n", focalLength);
        fprintf(stderr, "ipOffsetMultiplier: %f\n", ipOffsetMultiplier);
    }

    uint8_t* renderRow(int y) {
        this->x = w - 1;
        this->y = y;
        ThreadLocals *syncThread = 0;
        for (int i = numThreads; i--; ) {
            ThreadLocals &t(threads[i]);
            if (!t.sync) {
                sem_post(&t.restart);
            } else {
                syncThread = &t;
            }
        }
        if (syncThread) syncThread->renderThread();
        for (int i = numThreads; i--; ) {
            ThreadLocals &t(threads[i]);
            if (!t.sync) sem_wait(&t.ready);
        }
        return row;
    }

    inline int getWidth() {
        return w;
    }

    inline int getHeight() {
        return h;
    }
};

void* ThreadLocals::renderThread() {
    while (true) {
        if (!sync) {
            sem_wait(&restart);
        }
        while (true) {
            int x = __sync_fetch_and_sub(&renderer->x, 1);
            if (x < 0) break;
            renderer->renderPixel(random, x, renderer->y);
        }
        if (sync) {
            break;
        } else {
            sem_post(&ready);
        }
    }
}

int main() {
  Renderer renderer(3840, 2160, 4096);
  renderer.dumpParameters();
  printf("P6 %d %d 255 ", renderer.getWidth(), renderer.getHeight());
  int last = time(NULL);
  int start = last;
  for (int y=renderer.getHeight(); y--;) {
  	int current = time(NULL);
	if (y < renderer.getHeight() && current > last || y == 0) {
		int overall = current - start;
		int left = (float) overall * y / (renderer.getHeight() - y);
		int expected = (float) overall * renderer.getHeight() / (renderer.getHeight() - y);
		fprintf(stderr, "\r%6.2f%% %3d:%02d:%02d %3d:%02d:%02d %3d:%02d:%02d",
            100.0f-y*100.0f/renderer.getHeight(),
            overall / 3600, overall / 60 % 60, overall % 60,
            left / 3600, left / 60 % 60, left % 60,
            expected / 3600, expected / 60 % 60, expected % 60);
		last = current;
	}
    uint8_t *c = renderer.renderRow(y);
    fwrite(c, renderer.getWidth(), 3, stdout);
  }
  fprintf(stderr, "\n");
  return 0;
}
